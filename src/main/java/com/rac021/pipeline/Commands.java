
package com.rac021.pipeline ;

import java.awt.Color;
import java.awt.Font;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter ;
import java.net.URL ;
import java.io.File ;
import java.io.FileNotFoundException;
import java.nio.file.Path ;
import java.io.IOException ;
import java.io.InputStream ;
import java.io.InputStreamReader;
import java.io.OutputStream ;
import java.io.OutputStreamWriter ;
import java.net.HttpURLConnection ;
import org.apache.commons.io.FileUtils ;
import java.util.ArrayList ;
import java.util.List ;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.maven.cli.MavenCli;
import org.eclipse.jgit.api.Git;


/**
 *
 * @author ryahiaoui
 */
public class Commands {
    
   private static void swingProgress( String URL, String targetDirectory, String name ) {
   
        String fileName = URL.substring(URL.lastIndexOf('/') + 1, URL.length()) ;
        
        final JProgressBar jProgressBar = new JProgressBar();
       
        jProgressBar.setStringPainted(true);
        jProgressBar.setForeground(Color.BLUE);
        jProgressBar.setFont(new Font("Tahoma", Font.BOLD, 12 ) ) ;
        jProgressBar.setBackground(Color.white);
        
        jProgressBar.setMaximum(100000) ;
        JFrame frame = new JFrame(" Downloding " + fileName ) ;
        frame.setContentPane(jProgressBar);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(400, 70) ;
        frame.setAlwaysOnTop(true);
        frame.setVisible(true) ;

        try {
         
            URL url = new URL( URL );
            int size = url.openConnection().getContentLength() ;
            if( size > 0 ) System.out.println(" Size Jar - " + ( size/1000000 ) + " Mo " ) ; 
                    
            HttpURLConnection httpConnection = (HttpURLConnection) (url.openConnection());
            long completeFileSize = httpConnection.getContentLength();

            java.io.BufferedInputStream in = new java.io.BufferedInputStream(httpConnection.getInputStream());
            java.io.FileOutputStream fos = new java.io.FileOutputStream( targetDirectory + File.separator + fileName.trim() );
            java.io.BufferedOutputStream bout = new BufferedOutputStream( fos, 1024 ) ;
            byte[] data = new byte[1024] ;
            long downloadedFileSize = 0 ;
            int x = 0 ;
                   
            while ((x = in.read(data, 0, 1024)) >= 0) {
                downloadedFileSize += x ;

                // calculate progress
                final int currentProgress = (int) ((((double)downloadedFileSize) / ((double)completeFileSize)) * 100000d ) ;
                        
                        jProgressBar.setValue(currentProgress);
                        try {
                            Thread.sleep(1) ;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Commands.class.getName()).log(Level.SEVERE, null, ex);
                        }
                
                bout.write(data, 0, x );
            }
            bout.close();
            in.close();
            frame.dispose();
            
             if( name != null) {
                Path targetPath = new File(targetDirectory + File.separator + fileName).toPath() ;
                Path targetRenamedPath = new File(targetDirectory + File.separator + name).toPath() ;
                FileUtils.moveFile( targetPath.toFile(), targetRenamedPath.toFile()) ;
                System.out.println(" Finish Downloded : " + URL ) ;
              }
             
                     frame.dispose();
                } catch (FileNotFoundException e) {
                } catch (IOException e) {
                }
            }
        
   
  public static Path wget(String URL, String targetDirectory, String name ) throws IOException     {

    System.out.println(" Start Downloding : " + URL ) ;
    swingProgress( URL, targetDirectory , name) ;
    String fileName = URL.substring(URL.lastIndexOf('/') + 1, URL.length()) ;
    Path targetPath = new File(targetDirectory + File.separator + fileName).toPath() ;
    return targetPath ;
  }

  
  
  public static boolean mkdir(String directory ) {
    System.out.println(" Create Folder : " + directory ) ;
    File fDirectory = new File(directory) ;
    return fDirectory.mkdir() ;
  }
 
  public static void rm(String directory ) throws IOException {
    System.out.println(" Remove Folder : " + directory ) ;
     FileUtils.deleteDirectory(new File(directory)) ;
  }

  
  public static boolean gitClone( String REMOTE_URL, String localPath ) throws Exception {

      System.out.println("Cloning from " + REMOTE_URL + " to " + localPath) ;
      try (Git result = Git.cloneRepository()
                           .setURI(REMOTE_URL)
                           .setDirectory(new File(localPath))
                           .call()) {
	        // Note: the call() returns an opened repository already which needs to be closed to avoid file handle leaks!
	        System.out.println("Having repository: " + result.getRepository().getDirectory());
                return true ;
        } catch( Exception x) {
            x.printStackTrace();
            System.out.println(" Error : " +x.getMessage() ) ;
            return false ;
        }
  }
  
  public static boolean compile( String directory , String profile ) throws Exception {
    
    System.setProperty("maven.multiModuleProjectDirectory", directory) ;
    System.out.println("") ;
    System.out.println(" *** clean install " + directory ) ;
    
    MavenCli cli = new MavenCli() ;
    
    try {
      String[] commands = new String[]{"clean", "install", "assembly:single"} ;
      if(profile != null ) {
         commands = new String[]{ "-P" , profile , "clean", "install", "assembly:single" } ;
      }
      cli.doMain( commands , directory , System.out, System.out ) ;
      return true ;
    } catch( Exception ex ) {
        ex.printStackTrace();
        System.out.println(" Exception : " +ex.getMessage() ) ;
    }
    return false ;
  }
  
  public static void cp ( String fileSource, String folder ) throws IOException {
     FileUtils.copyFile( new File(fileSource), new File(folder));
  }
  
  public static boolean mv ( String fileSource, String folder, String name ) throws IOException {
      
      if(new File(fileSource).exists()) {
        if( name == null ) {
          FileUtils.moveFileToDirectory( new File(fileSource), new File(folder), true) ;
        }
        else {
          FileUtils.moveFile( new File(fileSource) ,
                             new File(folder.endsWith("/") ? folder + name : folder + "/" + name) ) ;
        }
        return true ;
      } else {
          System.err.println(" File " + fileSource + " not found ! " ) ;
          return false ;
      }
  }
  
  
    public static void curl( String url,  String operation) throws Exception {
        
        URL myURL = new URL(url ) ;
        HttpURLConnection myURLConnection = (HttpURLConnection)myURL.openConnection();
        myURLConnection.setRequestMethod(operation ) ; // PUT
        myURLConnection.setRequestProperty("Accept", "text/rdf+n3");
        myURLConnection.setUseCaches(false);
        myURLConnection.setDoInput(true);
        myURLConnection.setDoOutput(true);
        
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("query", "SELECT ?S ?P ?O WHERE { ?S ?P ?O . } LIMIT 1"));

        int maxiRretry = 100 ;
        int retry      = 0  ;
        
        while( retry < maxiRretry )        {
            try {
                myURLConnection.connect()  ;
                break ;
            } catch( Exception x) {
                Thread.sleep(500) ;
                System.out.println(" retry ... " + ++ retry ) ;
            }    
        }

        if( retry == maxiRretry ) {
            System.out.println(" ") ;
            System.out.println(" Endpoint Unreachable ! Program will be exited " ) ;
            System.out.println(" ") ;
            return ;
        }
        
        OutputStream os = myURLConnection.getOutputStream() ;
        BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(os, "UTF-8"));
        writer.write(params.get(0).toString());
        writer.flush();
        writer.close();
        os.close();
                
        InputStream response = myURLConnection.getInputStream();
       
        int ch ;
        StringBuilder sb   = new StringBuilder() ;

        while (( ch = response.read()) != -1) {
           sb.append((char) ch ) ;
        }
        response.close()     ;
        System.out.println("response.read()  ===> " + sb.toString() );
    
    }
    
  
    public static boolean compileProject( String localProjectLocation , 
                                          final String gitProject     , 
                                          String compiledProjectName  , 
                                          String finalProjectName     , 
                                          String targetFolder         ,
                                          String profile              ) throws Exception {
       boolean ok     ;
       rm(localProjectLocation.trim())                                  ;  
       mkdir(localProjectLocation.trim())                               ;
       ok = gitClone(gitProject.trim() , localProjectLocation.trim() )  ;
       if(ok) ok = compile(localProjectLocation.trim(), profile)        ;
       if(ok) ok = mv( localProjectLocation.trim() + "target/" + compiledProjectName.trim() , 
                       targetFolder.trim()      , 
                       finalProjectName.trim()) ;
       rm(localProjectLocation.trim())          ;
       return ok ;
    }
  
   public static void execJar( String[] args ) {
       
       System.out.println(" Running app ... " ) ;
        try {
             String  s ;
             Process p ;
             p = Runtime.getRuntime().exec(args) ;
            
             BufferedReader br = new BufferedReader ( new InputStreamReader(p.getInputStream())) ;
             while ((s = br.readLine()) != null) {
                 System.out.println(" " + s ) ;
             }
             p.waitFor();
             p.destroy();

         } catch (IOException | InterruptedException ex) {
             System.out.println( ex.getMessage()) ;
         }
         finally {
            System.out.println(" App Running ") ;
         }
   }
   
   public static void killProcess( int port ) throws IOException {
       
      Runtime rt = Runtime.getRuntime(); 
  
      try {
      
         System.out.println(" OS : " + System.getProperty("os.name") ) ;
         System.out.println(" Killing Process on Port " + port ) ;
      
         if (System.getProperty("os.name").toLowerCase().contains("windows") ) {
             Process p = rt.exec("netstat -a -n -o | findstr "+ port ) ;
             String kport ;
             BufferedReader br = new BufferedReader ( new InputStreamReader(p.getInputStream())) ;
             while ((kport = br.readLine()) != null) {
                 System.out.println(" " + kport ) ;
             }
             p.waitFor();
             p.destroy();
             
             p = rt.exec("taskkill /PID "+ kport + " /F" ) ;
             br = new BufferedReader ( new InputStreamReader(p.getInputStream())) ;
             while ((kport = br.readLine()) != null) {
                 System.out.println(" " + kport ) ;
             }
             p.waitFor();
             p.destroy();
             
         }
         else {
             Process p = rt.exec(" fuser -k " + port + "/tcp " ) ;
             String s ;
             BufferedReader br = new BufferedReader ( new InputStreamReader(p.getInputStream())) ;
             while ((s = br.readLine()) != null) {
                 System.out.println( s ) ;
             }
             p.waitFor();
             p.destroy();
         }
      } catch( Exception x) {
          System.out.println( x.getMessage()) ;
      }
      finally {
          System.out.println(" Killed " ) ;
      }
  }
   
}
