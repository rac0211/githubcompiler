
package com.rac021.pipeline ;

import static com.rac021.pipeline.Commands.* ;

/**
 *
 * @author ryahiaoui
 */
public class _00_Install {
    
    final static String LIBS                    = "/home/ryahiaoui/jaxy_2019/jax-Y-ws/Jaxy_Client_Compiler/compilation_test/" ;
   
    /* 
    INSTALLATION CONFIG 
    */
    final static String TMP_MAVEN_PROJECT       = LIBS + "TMP_INSTALL/"         ;
    final static String TARGET_PROJECT          = TMP_MAVEN_PROJECT + "target/" ;
 
    /*
    YED_GEN CONFIG 
    */
    final static String  YEDGEN_COMPILED_NAME  =  "yedGen_2.1-2.1-jar-with-dependencies.jar" ;
    final static String  YEDGEN_FINAL_NAME     =  "yedGen.jar" ;
    final static String  YEDGEN_GIT            =  "https://github.com/rac021/yedGen.git"  ;
   
    
    public static void main(String[] args) throws Exception {

      // curl("http://localhost:8888/blazegraph/namespace/data/sparql", "POST" ) ;
 
       rm(LIBS)    ;
       mkdir(LIBS) ;
    
       
       /* YED_GEN */
       boolean compileProject = compileProject( TMP_MAVEN_PROJECT      , 
                                                YEDGEN_GIT             ,
                                                YEDGEN_COMPILED_NAME   ,
                                                YEDGEN_FINAL_NAME      ,
                                                LIBS                   ,
                                                null                 ) ;
       System.out.println(" Finish ");
       
    }

}
